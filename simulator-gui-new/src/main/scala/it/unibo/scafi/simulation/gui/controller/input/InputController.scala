package it.unibo.scafi.simulation.gui.controller.input

import it.unibo.scafi.simulation.gui.controller.Controller
import it.unibo.scafi.simulation.gui.model.aggregate.AggregateWorld

/**
  * an input controller for aggregate world
  */
trait InputController extends Controller[AggregateWorld]

